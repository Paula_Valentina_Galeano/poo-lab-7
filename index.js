//Importar la clase express
import express from 'express';

//Crear un objeto express
const app = express();
let puerto = 3000;

 /**
  * Definir las rutas.
  */

//Ruta de Bienvenida
app.get("/Bienvenida", (req, res)=>{

    res.send("REPASO");
});

//Ruta para la operación sumar
app.get("/Sumar", (peticion, respuesta)=>{

    let resultado = Number (peticion.query.a) + Number (peticion.query.b);
    
    respuesta.send(resultado.toString());
});

//Ruta para la operación restar
app.get("/Restar", (peticion, respuesta)=>{
    
    let resultado = Number (peticion.query.a) - Number (peticion.query.b);
    
    respuesta.send(resultado.toString());
});

//Ruta para la operación multiplicar
app.get("/Multiplicar", (peticion, respuesta)=>{
    
    let resultado = Number (peticion.query.a) * Number (peticion.query.b);
    
    respuesta.send(resultado.toString());
});

//Ruta para la operación dividir
app.get("/Dividir", (peticion, respuesta)=>{
    
    if (Number (peticion.query.b)==0){
        respuesta.send("Error")
    }
    else{
        let resultado = Number (peticion.query.a) / Number (peticion.query.b);
        respuesta.send(resultado.toString());
    }
});

//Ruta para la operación modulo
app.get("/Modulo", (peticion, respuesta)=>{
    
    let resultado = Number (peticion.query.a) % Number (peticion.query.b);

    respuesta.send(resultado.toString());
});

//Iniciar un servidor
app.listen(puerto, ()=>{
    console.log("ESTA FUNCIONANDO EL SERVIDOR ");
});
